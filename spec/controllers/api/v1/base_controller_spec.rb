require 'rails_helper'

RSpec.describe Api::V1::BaseController, type: :controller do
  let!(:controller) { Api::V1::BaseController.new }
  before(:each) { allow(controller).to receive(:params).and_return({}) }

  describe 'private method #page' do
    it 'returns 1 as default' do
      expect(controller.send(:page)).to eq(1)
    end

    it 'returns the desired page' do
      allow(controller).to receive(:params).and_return({page: 2})

      expect(controller.send(:page)).to eq(2)
    end
  end

  describe 'private method #per_page' do
    it 'returns 10 as default' do
      expect(controller.send(:per_page)).to eq(10)
    end

    it 'returns the desired quantity of objects' do
      allow(controller).to receive(:params).and_return({per_page: 5})

      expect(controller.send(:per_page)).to eq(5)
    end
  end

  describe 'private method #distance' do
    it 'returns 2 as default' do
      expect(controller.send(:distance)).to eq(2)
    end

    it 'returns the desired km range to search for a ubs' do
      allow(controller).to receive(:params).and_return({distance: 5})

      expect(controller.send(:distance)).to eq(5)
    end
  end

  describe 'private method #pagination' do
    it 'creates a hash with passed collection' do
      create_list(:ubs, 10)
      allow(controller).to receive(:params).and_return({per_page: 2, page: 1})
      collection = Ubs.all.page(controller.params[:page]).per(controller.params[:per_page])

      result = controller.send(:pagination, collection)

      expect(result[:current_page]).to eq(1)
      expect(result[:next_page]).to eq(2)
      expect(result[:per_page]).to eq(2)
      expect(result[:previous_page]).to eq(nil)
      expect(result[:total_entries]).to eq(10)
    end

    it 'it changes when the page and per_page params changes' do
      create_list(:ubs, 10)
      allow(controller).to receive(:params).and_return({per_page: 5, page: 2})
      collection = Ubs.all.page(controller.params[:page]).per(controller.params[:per_page])

      result = controller.send(:pagination, collection)

      expect(result[:current_page]).to eq(2)
      expect(result[:next_page]).to eq(nil)
      expect(result[:per_page]).to eq(5)
      expect(result[:previous_page]).to eq(1)
      expect(result[:total_entries]).to eq(10)
    end
  end
end
