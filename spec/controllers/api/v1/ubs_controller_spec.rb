require 'rails_helper'

RSpec.describe Api::V1::UbsController, type: :controller do
  describe 'GET #find_ubs' do
    context 'search ubs for desired latitude and longitude' do
      it 'when it have any results' do
        create_list(:ubs, 5, latitude: -10.9112370014188, longitude: -37.0620775222768)

        get :find_ubs, params: { query: '-10.9112370014188,-37.0620775222768' }

        expect(json_response["entries"].size).to eq(5)
      end

      it 'when have per page params' do
        create_list(:ubs, 5, latitude: -10.9112370014188, longitude: -37.0620775222768)

        get :find_ubs, params: { query: '-10.9112370014188,-37.0620775222768', per_page: 1 }

        expect(json_response["entries"].size).to eq(1)
      end

      it 'when have per page and page params' do
        create_list(:ubs, 5, latitude: -10.9112370014188, longitude: -37.0620775222768)

        get :find_ubs, params: { query: '-10.9112370014188,-37.0620775222768', per_page: 2, page: 3 }

        expect(json_response["entries"].size).to eq(1)
      end

      it 'when have no results' do
        create_list(:ubs, 5, latitude: -10.9112370014188, longitude: -37.0620775222768)

        get :find_ubs, params: { query: '1,1' }

        expect(json_response["entries"]).to eq([])
      end
    end

    context 'when not passing query params' do
      it 'returns an empty array' do
        get :find_ubs, params: {}

        expect(json_response["error"]).to eq(I18n.t('find_ubs.error-query-empty'))
      end
    end
  end

  describe 'private method #location_params' do
    it 'convert the query to a hash' do
      controller = Api::V1::UbsController.new

      allow(controller).to receive(:params).and_return({query: "1,1"})

      expect(controller.send(:location_params)).to eq([1.0, 1.0])
    end

    it 'convert to empty query when params is empty or missing one param' do
      controller = Api::V1::UbsController.new

      allow(controller).to receive(:params).and_return({query: "1"})

      expect(controller.send(:location_params)).to eq({})
    end
  end

  describe 'private method #can_search_ubs?' do
    context 'when query params is filled correctly' do
      it 'returns true' do
        controller = Api::V1::UbsController.new

        allow(controller).to receive(:params).and_return({query: "1,1"})

        expect(controller.send(:can_search_ubs?)).to eq(true)
      end
    end

    context 'when query params is not filled correctly' do
      it 'returns false' do
        controller = Api::V1::UbsController.new

        allow(controller).to receive(:params).and_return({query: "1"})

        expect(controller.send(:can_search_ubs?)).to eq(false)
      end
    end
  end
end
