FactoryBot.define do
  factory :ubs do
    name { "UBS REAL PQ PAULO MANGABEIRA ALBERNAZ FILHO" }
    address { "RUA BARAO MELGACO" }
    city { "São Paulo" }
    phone { "1137582329" }
    latitude { -23.6099946498864 }
    longitude { -46.7057347297655 }
    score_size { 3 }
    score_adaptation_for_seniors { 3 }
    score_medical_equipment { 1 }
    score_medicine { 3 }
  end
end
