require 'rails_helper'

RSpec.describe Ubs, type: :model do
  context 'validations' do
    context 'presence' do
      it { expect(subject).to validate_presence_of(:name)}
      it { expect(subject).to validate_presence_of(:address) }
      it { expect(subject).to validate_presence_of(:city) }
      it { expect(subject).to validate_presence_of(:phone) }
      it { expect(subject).to validate_presence_of(:latitude) }
      it { expect(subject).to validate_presence_of(:longitude) }
      it { expect(subject).to validate_presence_of(:score_size) }
      it { expect(subject).to validate_presence_of(:score_adaptation_for_seniors) }
      it { expect(subject).to validate_presence_of(:score_medical_equipment) }
      it { expect(subject).to validate_presence_of(:score_medicine) }
    end
  end

  describe 'methods' do
    describe 'search by latitude and longitude' do
      context 'when finds any ubs for desired location' do
        it 'returns the found ubs' do
          ubs_collection = create_list(:ubs, 5, latitude: -10.9112370014188, longitude: -37.0620775222768)

          result = Ubs.near([-10.9112370014188, -37.0620775222768])

          expect(result.size).to eq(5)
        end
      end

      context 'when find no ubs for desired location' do
        it 'returns an empty array' do
          ubs_collection = create_list(:ubs, 5, latitude: -10.9112370014188, longitude: -37.0620775222768)

          result = Ubs.near([1,1])

          expect(result.size).to eq(0)
        end
      end

      context 'when location params is not passed' do
        it 'returns an empty array' do
          expect { Ubs.near }.to raise_error(ArgumentError)
        end
      end
    end
  end
end
