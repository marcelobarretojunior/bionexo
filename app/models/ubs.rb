class Ubs < ApplicationRecord
  reverse_geocoded_by :latitude, :longitude

  validates :name, presence: true
  validates :address, presence: true
  validates :city, presence: true
  validates :phone, presence: true
  validates :latitude, presence: true
  validates :longitude, presence: true
  validates :score_size, presence: true
  validates :score_adaptation_for_seniors, presence: true
  validates :score_medical_equipment, presence: true
  validates :score_medicine, presence: true
end
