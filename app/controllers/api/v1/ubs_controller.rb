module Api
  module V1
    class UbsController < BaseController
      def find_ubs
        if can_search_ubs?
          ubs = Ubs.near(location_params, distance).page(page).per(per_page)
          entries = ActiveModel::ArraySerializer.new(ubs, each_serializer: UbsSerializer)

          render json: pagination(ubs).merge(entries: entries), status: :ok
        else
          render json: { error: I18n.t('find_ubs.error-query-empty') }, status: :bad_request
        end
      end

      private

      def location_params
        query = params[:query]

        if query && query.split(',').size == 2
          query.split(',').map(&:to_f)
        else
          {}
        end
      end

      def can_search_ubs?
        location_params.size == 2
      end
    end
  end
end
