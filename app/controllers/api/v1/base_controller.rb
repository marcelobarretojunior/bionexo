module Api
  module V1
    class BaseController < ApplicationController
      private
      def per_page
        params[:per_page] || 10
      end

      def page
        params[:page] || 1
      end

      def distance
        # distance in km
        params[:distance] || 2
      end

      def pagination(collection)
        {
          current_page: collection.current_page,
          next_page: collection.next_page,
          previous_page: collection.prev_page,
          per_page: per_page,
          total_entries: collection.model.count
        }
      end
    end
  end
end
