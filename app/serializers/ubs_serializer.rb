class UbsSerializer < ActiveModel::Serializer
  attributes :id, :name, :address, :city, :phone, :geocode, :scores

  def scores
    {
      size: object.score_size,
      adaptation_for_seniors: object.score_adaptation_for_seniors,
      medical_equipment: object.score_medical_equipment,
      medicine: object.score_medicine
    }
  end

  def geocode
    {
      lat: object.latitude,
      long: object.longitude
    }
  end
end