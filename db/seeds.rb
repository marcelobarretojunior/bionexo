require 'csv'
require_relative './seeds_helper'

ubs_file = File.join(Rails.root, 'ubs', 'ubs.csv')
csv = CSV.read(ubs_file)

puts 'Importing UBS, please hold on a minute'

csv.each do |row|
  Ubs.create(ubs_attributes(row))
end
