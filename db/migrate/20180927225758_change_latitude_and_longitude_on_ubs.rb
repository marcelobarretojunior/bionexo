class ChangeLatitudeAndLongitudeOnUbs < ActiveRecord::Migration[5.2]
  def change
    change_column(:ubs, :latitude, :float, precision: 10, scale: 6)
    change_column(:ubs, :longitude, :float, precision: 10, scale: 6)
  end
end
