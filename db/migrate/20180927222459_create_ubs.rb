class CreateUbs < ActiveRecord::Migration[5.2]
  def change
    create_table :ubs do |t|
      t.string :name
      t.string :address
      t.string :city
      t.string :phone
      t.decimal :latitude
      t.decimal :longitude
      t.integer :score_size
      t.integer :score_adaptation_for_seniors
      t.integer :score_medical_equipment
      t.integer :score_medicine

      t.timestamps
    end
  end
end
