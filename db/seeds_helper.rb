def classify_treatment(treatment)
  case treatment.squish
  when 'Desempenho muito acima da média'
    3
  when 'Desempenho mediano ou um pouco abaixo da média'
    2
  else
    1
  end
end

def ubs_attributes(row)
  {
    name: row[4],
    address: row[5],
    city: row[7],
    phone: row[8],
    latitude: row[0].to_f,
    longitude: row[1].to_f,
    score_size: classify_treatment(row[9]),
    score_adaptation_for_seniors: classify_treatment(row[10]),
    score_medical_equipment: classify_treatment(row[11]),
    score_medicine: classify_treatment(row[12])
  }
end
