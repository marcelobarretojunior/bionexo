FROM ruby:2.4.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /bionexo
WORKDIR /bionexo
COPY Gemfile /bionexo/Gemfile
COPY Gemfile.lock /bionexo/Gemfile.lock
RUN bundle install
# COPY . /bionexo

ADD . .
CMD ["puma"]