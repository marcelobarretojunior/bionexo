# Bionexo Challenge

![Bionexo](https://scontent.fcaw3-1.fna.fbcdn.net/v/t1.0-1/p200x200/13051699_1076859035727069_7784965190526867428_n.png?_nc_cat=104&oh=71b23a90a404fae36e65b525c4f618c4&oe=5C1D8C22)

## Setup
Considering that you already have Docker and Docker Compose installed on your machine, follow the steps below:

```sh
  docker-compose build
  docker-compose run --rm web rails db:reset RAILS_ENV=development
  docker-compose run --rm web rails db:reset RAILS_ENV=test
  docker-compose up
```

If not, you can follow the [steps here for Docker](https://docs.docker.com/install/) and [here for Docker Compose](https://docs.docker.com/compose/) and then run the commands above.

## Troubleshooting
If you are getting any problem with permissions, try the follow command:

```sh
sudo chown -R $USER:$USER .
```
